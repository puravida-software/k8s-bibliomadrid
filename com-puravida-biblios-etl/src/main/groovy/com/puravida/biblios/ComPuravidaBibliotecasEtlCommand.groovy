package com.puravida.biblios

import com.puravida.biblios.model.Prestamo
import com.puravida.biblios.model.PrestamoRepository
import com.puravida.biblios.model.Task
import com.puravida.biblios.model.TaskRepository
import io.micronaut.configuration.picocli.PicocliRunner
import picocli.CommandLine.Command
import picocli.CommandLine.Option

import javax.inject.Inject

@Command(name = 'com-puravida-bibliotecas-etl', description = '...',
        mixinStandardHelpOptions = true)
class ComPuravidaBibliotecasEtlCommand implements Runnable {

    @Option(names = ['-v', '--verbose'], description = '...')
    boolean verbose

    @Option(names = ['-y', '--year'], description = 'year', required = true)
    String year
    @Option(names = ['-m', '--month'], description = 'month', required = true)
    String month

    static void main(String[] args) throws Exception {
        PicocliRunner.run(ComPuravidaBibliotecasEtlCommand.class, args)
    }

    @Inject
    PrestamoRepository prestamoRepository

    @Inject
    TaskRepository taskRepository

    void run() {

        String url = "https://datos.madrid.es/datosabiertos/CULTURA/BIBLIOTECAS_PRESTAMOS/$year/$month/bibliotecas_prestamos_${year}${month}.txt"
        String name = url.split('/').last().split('\\.').first()

        if( taskRepository.findById(name) ){
            println "$f.name ya ha sido cargado"
            return
        }

        File f = File.createTempFile("prestamos","txt")
        f.bytes = url.toURL().bytes

        int lines=0
        List<Prestamo> list = []
        f.eachLine 'iso-8859-1',{ l, i ->
            if( i == 1 )
                return
            List<String>ll = l.split('\t') as List<String>
            while(ll.size() < 10)
                ll.add('')
            list.add( new Prestamo(
                    prbarc:ll[0]?.trim(),
                    prprsu:ll[1]?.trim(),
                    prcolp:ll[2]?.trim(),
                    prcocp:ll[3]?.trim(),
                    prlebi:ll[4]?.trim(),
                    prlesu:ll[5]?.trim(),
                    pradul:ll[6]?.trim(),
                    prfpre:ll[7]?.trim(),
                    prcocs:ll[8]?.trim(),
                    tititu:ll[9]?.trim(),
                    tiauto:ll[10]?.trim(),
                    tiisxn:ll[11]?.trim(),
            ))
            if( list.size() > 100){
                println i
                prestamoRepository.saveAll(list)
                list.clear()
            }
            lines++
        }
        prestamoRepository.saveAll(list)
        lines+=list.size()
        taskRepository.save(new Task(file:name, when: new Date(), lines: lines))
    }
}
