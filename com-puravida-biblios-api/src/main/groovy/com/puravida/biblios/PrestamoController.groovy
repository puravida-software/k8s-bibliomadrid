package com.puravida.biblios

import com.puravida.biblios.model.Prestamo
import com.puravida.biblios.model.PrestamoBySucursal
import com.puravida.biblios.model.PrestamoRepository
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.data.model.Slice
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Maybe
import io.reactivex.Single

import javax.annotation.Nullable

@Controller('/prestamos')
class PrestamoController {

    PrestamoRepository prestamoRepository

    PrestamoController(PrestamoRepository prestamoRepository){
        this.prestamoRepository = prestamoRepository
    }

    @Get('/{?requestPage*}')
    Page<Prestamo> index(final RequestPage requestPage){
        prestamoRepository.list(Pageable.from(requestPage.offset, requestPage.size ?: 10))
    }

    @Get('/title/{title}{?exact}')
    Single<List<PrestamoBySucursal>> findByTitle(final String title, @Nullable Boolean exact){

        Single.create { emitter ->
            String search = exact ? title : '%title%'
            List<PrestamoBySucursal> ret = []
            prestamoRepository.findDistinctPrprsu().each { prprsu ->
                ret.add( new PrestamoBySucursal(
                        prprsu: prprsu,
                        count:  prestamoRepository.countByTitituIlikeAndPrprsu(search, prprsu),
                        tititu: title
                ))
            }
            emitter.onSuccess(ret)
        }
    }

    @Get('/author/{author}{?requestPage*}')
    Page<Prestamo> findByAuthor(final String author, final RequestPage requestPage){
        prestamoRepository.findByTiautoIlike("%$author%",Pageable.from(requestPage.offset, requestPage.size ?: 10))
    }

}
