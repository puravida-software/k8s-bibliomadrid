package com.puravida.biblios.model
import io.micronaut.data.annotation.AutoPopulated

import javax.annotation.Nullable
import javax.persistence.*

@Entity
class Task {

    @Id
    String file

    Date when
    int lines

}
