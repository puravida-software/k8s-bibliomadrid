package com.puravida.biblios.model

import io.micronaut.core.annotation.Introspected

@Introspected
class PrestamoBySucursal {

    long count
    String tititu
    String prprsu

}
