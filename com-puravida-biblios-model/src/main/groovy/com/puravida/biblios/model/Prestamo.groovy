package com.puravida.biblios.model

import io.micronaut.data.annotation.AutoPopulated

import javax.annotation.Nullable
import javax.persistence.*

@Entity
class Prestamo {

    @Id
    @AutoPopulated
    UUID id;

    String prbarc
    String prprsu
    String prcolp
    String prcocp
    String prlebi
    String prlesu
    String pradul
    String prfpre
    String prcocs
    String tititu
    @Nullable
    String tiauto
    @Nullable
    String tiisxn
}
