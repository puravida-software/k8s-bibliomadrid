package com.puravida.biblios.model

import io.micronaut.data.annotation.Query
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.data.model.Slice
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.repository.CrudRepository

import java.util.concurrent.CompletableFuture

@JdbcRepository(dialect = Dialect.POSTGRES)
interface PrestamoRepository extends CrudRepository<Prestamo, Long> {

    Page<Prestamo> list(Pageable pageable)

    Page<Prestamo> findByTitituIlike(String title, Pageable pageable)

    Page<Prestamo> findByTiautoIlike(String autor, Pageable pageable)

    List<String> findDistinctPrprsu()

    long countByTitituIlikeAndPrprsu(String title, String prprsu)

}
